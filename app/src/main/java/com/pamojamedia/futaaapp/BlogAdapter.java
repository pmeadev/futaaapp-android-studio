package com.pamojamedia.futaaapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.curioustechizen.ago.RelativeTimeTextView;

import java.util.List;


public class BlogAdapter extends RecyclerView.Adapter<BlogAdapter.MyViewHolder> {

    public static LayoutInflater blogInflater;
    public List<BlogData> blogList;
    private Context context;

    public BlogAdapter(Context context, List<BlogData> blogList) {
        blogInflater = LayoutInflater.from(context);
        this.blogList = blogList;
    }

    @Override
    public BlogAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = blogInflater.inflate(R.layout.blog_custom_row, parent, false);
        MyViewHolder holder = null;
        holder = new MyViewHolder(view, context);
        return holder;
    }

    @Override
    public void onBindViewHolder(final BlogAdapter.MyViewHolder holder, int position) {

        final BlogData current = blogList.get(position);
        holder.imgUrl = current.getImgUrl();
        holder.id = current.getId();
        holder.detailsUrl = current.getDetailsUrl();

        //glide image loading
        //glide
        if (holder.context != null) {
            Glide.with(holder.context).load(current.getImgUrl())
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            holder.blogProgressBar.setVisibility(View.GONE);
                            return false;
                        }
                    }).override(800, 530)
                    .placeholder(R.drawable.placeholder)
                    .into(holder.blogImage);
        }
        //end glide

        //glide image loading

        holder.blogTitle.setText(current.blogTitle);
        holder.blogCategory.setText(current.blogCategory);
        holder.blogTime.setText(current.blogTime);

        //new variables
        holder.detailsUrl = current.getDetailsUrl();

        holder.paragraph = current.getParagraph();


    }


    @Override
    public int getItemCount() {

        return (null != blogList ? blogList.size() : 0);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        //variable for selecting the view type
        public int view_type;
        public Context context;
        RelativeTimeTextView blogTime;
        TextView blogTitle, blogCategory;
        TextView headerTitle, headerCategory, headerTime;
        ImageView blogImage, headerImage;
        String paragraph = "";
        String detailsUrl = "";
        String imgUrl = "";
        ProgressBar blogProgressBar;
        String base_url = "http://www.futaa.com/json/7wviyWzZAoSZBzDofzArBwvSa9tSZBB009VDqyPy/article/";
        int id;

        public MyViewHolder(View itemView, Context c) {
            super(itemView);
            context = itemView.getContext();
            itemView.setClickable(true);
            //id = itemView.getId();
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, NewsDetails.class);
                    //bundling
                    Bundle bundle = new Bundle();
                    bundle.putString("imgUrl", imgUrl);
                    bundle.putString("detailsUrl", detailsUrl);
                    bundle.putString("paragraph", paragraph);
                    bundle.putString("newsTime", blogTime.getText().toString());
                    bundle.putString("newsCategory", blogCategory.getText().toString());
                    bundle.putString("newsTitle", blogTitle.getText().toString());
                    bundle.putInt("id", id);
                    bundle.putString("base_url", base_url);
                    intent.putExtras(bundle);
                    context.startActivity(intent);

                }
            });
            //images
            blogImage = (ImageView) itemView.findViewById(R.id.blogImage);
            // mAttacher = new PhotoViewAttacher(newsImage);
            blogProgressBar = (ProgressBar) itemView.findViewById(R.id.blogProgressBar);
            //title
            blogTitle = (TextView) itemView.findViewById(R.id.blogTitle);
            //time ago library
            blogTime = (RelativeTimeTextView) itemView.findViewById(R.id.blogTime);//Or just me to simplify work!
//                String inputStr = "16-03-2016 06:34 AM";
//                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
//                long inputDate = dateFormat.parse(inputStr).getTime();
//                newsTime.setReferenceTime(inputDate);


            //category
            blogCategory = (TextView) itemView.findViewById(R.id.blogCategory);
            blogTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, NewsDetails.class);
                    //bundling
                    Bundle bundle = new Bundle();

                    //strings
                    bundle.putString("imgUrl", imgUrl);
                    bundle.putString("detailsUrl", detailsUrl);
                    bundle.putString("paragraph", paragraph);
                    bundle.putString("newsTime", blogTime.getText().toString());
                    bundle.putString("newsCategory", blogCategory.getText().toString());
                    bundle.putString("newsTitle", blogTitle.getText().toString());
                    bundle.putString("base_url", base_url);
                    bundle.putInt("id", id);
                    intent.putExtras(bundle);
                    context.startActivity(intent);

                }
            });



            blogCategory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent readOnline = new Intent(context, ReadOnline.class);
                    Bundle b = new Bundle();
                    b.putString("detailsUrl", detailsUrl);
                    b.putInt("id", id);
                    readOnline.putExtras(b);
                    context.startActivity(readOnline);
                }
            });
        }
    }
}







