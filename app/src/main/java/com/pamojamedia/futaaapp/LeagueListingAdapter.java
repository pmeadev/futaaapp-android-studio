package com.pamojamedia.futaaapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

class LeagueListingAdapter extends RecyclerView.Adapter<LeagueListingAdapter.MyViewHolder1> {

    public Context context;
    public static LayoutInflater liveInflater;
    private List<LeagueListingData> listTable = new ArrayList<>();

    public LeagueListingAdapter(Context context, List<LeagueListingData> listTable) {
        liveInflater = LayoutInflater.from(context);
        this.context = context;
        this.listTable = listTable;
    }

    public void setLiveDataList(ArrayList<LiveData> liveDataList) {
        this.listTable = listTable;
        notifyItemRangeChanged(0, liveDataList.size());
    }

    @Override
    public MyViewHolder1 onCreateViewHolder(ViewGroup parent,  int viewType) {

        Context context = parent.getContext();
        LayoutInflater liveInflater = LayoutInflater.from(context);


        View view = liveInflater.inflate(R.layout.league_table_row, parent, false);
        MyViewHolder1 holder = null;
        try {
            holder = new MyViewHolder1(view, viewType, context);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return holder;

    }

    @Override
    public void onBindViewHolder(MyViewHolder1 holder, int i) {
        LeagueListingData listingData = listTable.get(i);
        holder.rank.setText(listingData.getRank()+"");
        holder.teamName.setText(listingData.getTeamName());
        holder.played.setText(listingData.getPlayed()+"");
        holder.goals.setText(listingData.getGoals()+"");
        holder.conceded.setText(listingData.getConceded()+"");
        holder.goalDiff.setText(listingData.getGoalDiff()+"");
        holder.points.setText(listingData.getPoints()+"");
      }



    @Override
    public int getItemCount() {
        return listTable == null ? 0 : listTable.size();
    }

    //get view type
    @Override
    public int getItemViewType(int position) {
        return 0;

    }

    public static class MyViewHolder1 extends RecyclerView.ViewHolder {

        public int view_type;
        public Context context;
        public TextView rank, teamName, played, goals, conceded, goalDiff, points;
        public ProgressBar progressBar;


        public MyViewHolder1(View itemView, int viewType, Context c) throws ParseException {
            super(itemView);
            context = itemView.getContext();
            itemView.setClickable(true);
            rank = (TextView) itemView.findViewById(R.id.rank);
            teamName = (TextView) itemView.findViewById(R.id.teamName);
            played = (TextView) itemView.findViewById(R.id.played);
            goals = (TextView) itemView.findViewById(R.id.goals);
            conceded = (TextView) itemView.findViewById(R.id.conceded);
            goalDiff = (TextView) itemView.findViewById(R.id.goalDiff);
            points = (TextView) itemView.findViewById(R.id.points);

            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
        }
    }

}







