package com.pamojamedia.futaaapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

class LeaguesAdapter extends RecyclerView.Adapter<LeaguesAdapter.MyViewHolder1> {


    public Context context;
    public LayoutInflater liveInflater;
    public List<LeaguesData> leaguesDataList = new ArrayList<>();

    public LeaguesAdapter(Context context, List<LeaguesData> leaguesDataList) {
        liveInflater = LayoutInflater.from(context);
        this.context = context;
        this.leaguesDataList = leaguesDataList;
    }

    public LeaguesAdapter() {

    }

    @Override
    public MyViewHolder1 onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater liveInflater = LayoutInflater.from(context);


        View view = liveInflater.inflate(R.layout.leagues_custom_row, parent, false);
        MyViewHolder1 holder = null;
        try {
            holder = new MyViewHolder1(view, viewType, context);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return holder;

    }

    @Override
    public void onBindViewHolder(MyViewHolder1 holder, int i) {
        LeaguesData  leaguescurrent = leaguesDataList.get(i);

       holder.tvLigName.setText(leaguescurrent.getLeagueName());
        holder.tvleagueUrl.setText(leaguescurrent.getLeagueTitle());


    }


    @Override
    public int getItemCount() {
        return leaguesDataList == null ? 0 : leaguesDataList.size();
    }

    //get view type
    @Override
    public int getItemViewType(int position) {
        return 0;

    }

    public static class MyViewHolder1 extends RecyclerView.ViewHolder {

        public int view_type;
        public Context context;
        public TextView tvleagueUrl, tvLigName;
        public String leagueUrl;
        public String leagueName;


        public MyViewHolder1(View itemView, int viewType, Context c) throws ParseException {
            super(itemView);
            context = itemView.getContext();
            itemView.setClickable(true);

            tvLigName = (TextView)itemView.findViewById(R.id.leagueName);
            tvleagueUrl = (TextView)itemView.findViewById(R.id.leagueUrl);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, LeagueDetails.class);
                    Bundle b = new Bundle();
                    b.putString("lname", tvleagueUrl.getText().toString());
                    b.putString("ltitle", tvLigName.getText().toString());
                    i.putExtras(b);
                    context.startActivity(i);

                }
            });


        }


    }

}







