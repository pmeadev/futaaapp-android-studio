package com.pamojamedia.futaaapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

public class LiveDetails extends AppCompatActivity {

    public static String pliveFirstName, pliveSecondName, pliveCategory, eventUrl;
    public static String EVENT_URL = "http://www.futaa.com/json/7wviyWzZAoSZBzDofzArBwvSa9tSZBB009VDqyPy/event/";
    ImageView liveFirstImage, liveSecondImage;
    TextView liveTime, liveFirstName, liveSecondName, liveCategory;

    //tabs
    SlidingTabLayout liveTabs;
    LivePagerAdapter liveViewPagerAdapter;
    ViewPager liveViewPager;
    CharSequence liveTitles[] = { "Details",  "Comments", "Live" };
    int noOfTabs = 3;
    String api_url;
    int api_id;


    public int getApi_id() {
        return api_id;
    }

    public void setApi_id(int api_id) {
        this.api_id = api_id;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //initializing
        liveFirstImage = (ImageView) findViewById(R.id.liveFirstImage);
        liveSecondImage = (ImageView) findViewById(R.id.liveSecondImage);
        liveTime = (TextView) findViewById(R.id.liveTime);
        liveFirstName = (TextView) findViewById(R.id.liveFirstName);
        liveSecondName = (TextView) findViewById(R.id.liveSecondName);
        liveCategory = (TextView) findViewById(R.id.liveCategory);

        //getting values from bundles
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String awayTeamImgUrl = extras.getString("awayTeamImgUrl");
            String homeTeamImgUrl = extras.getString("homeTeamImgUrl");
            String eventInfo = extras.getString("eventInfo");
            eventUrl = "http//futaa.com" + extras.getString("eventUrl");
            EVENT_URL = eventUrl;
            api_id = extras.getInt("id");


            // loading images
            // loading
            //glide
            Glide.with(this).load(homeTeamImgUrl)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            //progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    }).override(600, 600)
                    .centerCrop()
                    .crossFade()
                    .into(liveFirstImage);
            //glide

            //glide
            Glide.with(this).load(awayTeamImgUrl)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            //progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    }).override(600, 600)
                    .centerCrop()
                    .crossFade()
                    .into(liveSecondImage);
            //glide


            //getting texts
            pliveFirstName = extras.getString("liveFirstName");
            pliveSecondName = extras.getString("liveSecondName");
            pliveCategory = extras.getString("liveCategory");

            //setting the values respectively
            liveFirstName.setText(pliveFirstName);
            liveSecondName.setText(pliveSecondName);
            liveCategory.setText(eventUrl);
            liveCategory.setVisibility(View.GONE);
            liveTime.setText(eventInfo);

            //passing values to fragment

        }



        //adapter
        liveViewPagerAdapter = new LivePagerAdapter(getSupportFragmentManager(), liveTitles, noOfTabs);
        liveViewPager = (ViewPager) findViewById(R.id.liveDetailsPager);
        liveViewPager.setAdapter(liveViewPagerAdapter);
        liveViewPager.setOffscreenPageLimit(3);

        //sliding tabs
        liveTabs = (SlidingTabLayout) findViewById(R.id.liveDetailsTabs);
        liveTabs.setDistributeEvenly(true);
        liveTabs.setViewPager(liveViewPager);
        liveTabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return ContextCompat.getColor(getBaseContext(), R.color.whiteTextColor);
            }
        });

        liveTabs.setViewPager(liveViewPager);


    }




    public static LiveLive newInstance(String eventUrl) {
        LiveLive livliv = new LiveLive();
        Bundle args = new Bundle();
        args.putString("eventUrl", eventUrl);

        livliv.setArguments(args);
        return livliv;
    }


    //menus
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_comments, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}

