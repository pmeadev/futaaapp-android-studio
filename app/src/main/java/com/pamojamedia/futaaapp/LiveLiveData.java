package com.pamojamedia.futaaapp;

/**
 * Created by Edwin on 3/1/2016.
 */
public class LiveLiveData implements Comparable<LiveLiveData> {
    int livescore_away = 1;
    int elapsed = 10;
    int team = 0;
    int  home_id= 9857;
    int  livescore_home=0;
    int away_id= 9876;
    String  home_team ="Bologna";
    String away_team= "Hellas Verona";
    String league ="Italy-Serie A 2015/2016";
    String time = "04/04-2016 21:50";
    String status = "Finished";
    String code = "subst";
    String iconUrl = "/img/events/20.gif";
    String details = "Franco Brienza for Saphir Taider";

    @Override
    public int compareTo(LiveLiveData a) {
        if(this.elapsed < a.elapsed)return 1;
        else if (this.elapsed > a.elapsed) return -1;
        else return 1;
    }
}
