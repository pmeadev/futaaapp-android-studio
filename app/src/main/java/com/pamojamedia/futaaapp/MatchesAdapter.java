package com.pamojamedia.futaaapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.github.curioustechizen.ago.RelativeTimeTextView;
import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;



public class MatchesAdapter extends RecyclerView.Adapter<MatchesAdapter.MyViewHolder> {

    // PhotoViewAttacher mAttacher;


    public static final int TYPE_HEAD = 0;
    public static final int TYPE_LIST = 1;
    public static final int TYPE_AD = 3;
    //variable
    public Context context;
    public LayoutInflater newsInflater;
    List<NewsData> data = Collections.EMPTY_LIST;


    public MatchesAdapter(Context context, List<NewsData> data) {
        newsInflater = LayoutInflater.from(context);
        this.data = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_LIST) {
            View view = newsInflater.inflate(R.layout.news_custom_row, parent, false);
            MyViewHolder holder = null;
            try {
                holder = new MyViewHolder(view, viewType, context);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return holder;
        } else if (viewType == TYPE_HEAD) {
            View view = newsInflater.inflate(R.layout.news_custom_header, parent, false);
            MyViewHolder holder = null;
            try {
                holder = new MyViewHolder(view, viewType, context);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return holder;
        } else if (viewType == TYPE_AD) {
            View view = newsInflater.inflate(R.layout.advert_view, parent, false);
            MyViewHolder holder = null;
            try {
                holder = new MyViewHolder(view, viewType, context);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return holder;
        }

        return null;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NewsData current = data.get(position);
        if (holder.view_type == TYPE_LIST) {

            //setting the news items
            holder.newsImage.setImageResource(current.newsImage);
            // mAttacher.update();
            holder.newsTitle.setText(current.newsTitle);
            holder.newsCategory.setText(current.newsCategory);
            //setting the time
            holder.newsTime.setText(current.newsTime);


            //  handling clicks for item view


        } else if (holder.view_type == TYPE_HEAD) {

            holder.headerCategory.setText(current.headerCategory);
            holder.headerTitle.setText(current.headerTitle);
            holder.headerTime.setText(current.headerTime);
            holder.headerTime.setVisibility(View.GONE);
        } else if (holder.view_type == TYPE_AD) {
            //

        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    //get view type
    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEAD;
        } else if (position == 1) {
            return TYPE_LIST;
        } else if (position == 5) {
            return TYPE_LIST;
        } else if (position == 2 || position == 8 || position == 14 || position == 20 || position == 26 || position == 32 || position == 38) {
            return TYPE_AD;
        } else {
            return TYPE_LIST;
        }

    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        //variable for selecting the view type
        public int view_type;
        public Context context;
        RelativeTimeTextView newsTime;
        TextView newsTitle, newsCategory;
        TextView headerTitle, headerCategory, headerTime;
        ImageView newsImage, headerImage;


        public MyViewHolder(View itemView, int viewType, Context c) throws ParseException {
            super(itemView);
            context = itemView.getContext();
            itemView.setClickable(true);
            if (viewType == TYPE_LIST) {

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int path = R.id.newsImage;

                        newsImage.buildDrawingCache();
                        Bitmap newsHeaderImage = newsImage.getDrawingCache();
                        BitmapFactory.decodeResource(context.getResources(), R.id.newsImage);
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        newsHeaderImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byte[] firstImage = stream.toByteArray();


                        //intent
                        Intent intent = new Intent(context, NewsDetails.class);
                        //bundling
                        Bundle bundle = new Bundle();
                        //adding bitmaps to intent.
                        intent.putExtra("newsImage", firstImage);
                        bundle.putInt("newsImageId", path);

                        //strings
                        bundle.putString("newsTime", newsTime.getText().toString());
                        bundle.putString("newsCategory", newsCategory.getText().toString());
                        bundle.putString("newsTitle", newsTitle.getText().toString());
                        intent.putExtras(bundle);
                        context.startActivity(intent);

                    }
                });
                //images
                newsImage = (ImageView) itemView.findViewById(R.id.newsImage);

                // mAttacher = new PhotoViewAttacher(newsImage);

                //title
                newsTitle = (TextView) itemView.findViewById(R.id.newsTitle);


                //time ago library
                newsTime = (RelativeTimeTextView) itemView.findViewById(R.id.newsTime);//Or just me to simplify work!
                String inputStr = "16-02-2016 06:34 AM";
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
                long inputDate = dateFormat.parse(inputStr).getTime();
                newsTime.setReferenceTime(inputDate);

                //category
                newsCategory = (TextView) itemView.findViewById(R.id.newsCategory);
                view_type = 1;


            } else if (viewType == TYPE_HEAD) {

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int path = R.id.newsImage;

                        headerImage.buildDrawingCache();
                        Bitmap newsHeaderImage = headerImage.getDrawingCache();
                        BitmapFactory.decodeResource(context.getResources(), R.id.headerImage);
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        newsHeaderImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byte[] firstImage = stream.toByteArray();


                        //intent
                        Intent intent = new Intent(context, NewsDetails.class);
                        //bundling
                        Bundle bundle = new Bundle();
                        //adding bitmaps to intent.
                        intent.putExtra("newsImage", firstImage);
                        bundle.putInt("newsImageId", path);

                        //strings
                        bundle.putString("newsTime", headerTime.getText().toString());
                        bundle.putString("newsCategory", headerCategory.getText().toString());
                        bundle.putString("newsTitle", headerTitle.getText().toString());
                        intent.putExtras(bundle);
                        context.startActivity(intent);

                    }
                });

                //images
                headerImage = (ImageView) itemView.findViewById(R.id.headerImage);
                // mAttacher = new PhotoViewAttacher(headerImage);

                //title
                headerTitle = (TextView) itemView.findViewById(R.id.headerTitle);


                //time ago library
                headerTime = (RelativeTimeTextView) itemView.findViewById(R.id.headerTime);
                headerTime.setVisibility(View.GONE);
                //Or just me to simplify work!


                //category
                headerCategory = (TextView) itemView.findViewById(R.id.headerCategory);
                view_type = 0;


            } else if (viewType == TYPE_AD) {
                view_type = 3;
            }


        }


    }


}







