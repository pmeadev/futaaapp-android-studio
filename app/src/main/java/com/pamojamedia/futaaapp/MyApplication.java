package com.pamojamedia.futaaapp;

import android.app.Application;
import android.content.Context;

/**
 * Created by Edwin Amunga on 5/5/2016.
 */
public class MyApplication extends Application {
    public static MyApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance =this;
    }

    public static MyApplication getsInstance(){
        return sInstance;
    }

    public static Context getAppContext(){
        return sInstance.getApplicationContext();
    }
}
