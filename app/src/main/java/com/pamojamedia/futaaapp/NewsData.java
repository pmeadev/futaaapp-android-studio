package com.pamojamedia.futaaapp;

/**
 * Created by Edwin on 1/25/2016.
 */
public class NewsData {

    //header news
    String headerImage;
    String headerTitle;
    String headerCategory;
    String headerTime;


    int newsImage;
    String newsTitle;
    String newsCategory;
    String newsTime;
    String detailUrl;

    String paragraph;


    //
    String imgUrl;
    int pageNo;
    int id;
    String is_pinned;




    public NewsData() {
    }

    public NewsData(String headerImage, String headerCategory, String headerTitle, int newsImage, String headerTime, String newsTitle, String newsCategory, String newsTime, String detailUrl, String paragraph, String imgUrl, int pageNo, int id, String is_pinned) {
        this.headerImage = headerImage;
        this.headerCategory = headerCategory;
        this.headerTitle = headerTitle;
        this.newsImage = newsImage;
        this.headerTime = headerTime;
        this.newsTitle = newsTitle;
        this.newsCategory = newsCategory;
        this.newsTime = newsTime;
        this.detailUrl = detailUrl;
        this.paragraph = paragraph;
        this.imgUrl = imgUrl;
        this.pageNo = pageNo;
        this.id = id;
        this.is_pinned = is_pinned;
    }

    public String getDetailUrl() {
        return detailUrl;
    }

    public void setDetailUrl(String detailUrl) {
        this.detailUrl = detailUrl;
    }

    public String getParagraph() {
        return paragraph;
    }

    public void setParagraph(String paragraph) {
        this.paragraph = paragraph;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public String getIs_pinned() {
        return is_pinned;
    }

    public void setIs_pinned(String is_pinned) {
        this.is_pinned = is_pinned;
    }

    public String getHeaderImage() {
        return headerImage;
    }

    public void setHeaderImage(String headerImage) {
        this.headerImage = headerImage;
    }

    public String getHeaderTitle() {
        return headerTitle;
    }

    public void setHeaderTitle(String headerTitle) {
        this.headerTitle = headerTitle;
    }

    public String getHeaderCategory() {
        return headerCategory;
    }

    public void setHeaderCategory(String headerCategory) {
        this.headerCategory = headerCategory;
    }

    public String getHeaderTime() {
        return headerTime;
    }

    public void setHeaderTime(String headerTime) {
        this.headerTime = headerTime;
    }

    public int getNewsImage() {
        return newsImage;
    }

    public void setNewsImage(int newsImage) {
        this.newsImage = newsImage;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsCategory() {
        return newsCategory;
    }

    public void setNewsCategory(String newsCategory) {
        this.newsCategory = newsCategory;
    }

    public String getNewsTime() {
        return newsTime;
    }

    public void setNewsTime(String newsTime) {
        this.newsTime = newsTime;
    }
}



