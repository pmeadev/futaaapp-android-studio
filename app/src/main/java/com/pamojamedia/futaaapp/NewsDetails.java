package com.pamojamedia.futaaapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class NewsDetails extends AppCompatActivity {

    public static String base_url;
    ViewPager pager;
    ViewPagerAdapter adapter;
    SlidingTabLayout tabs;
    CharSequence Titles[] = {"Comments"};
    int Numboftabs = 1;
    SpannableStringBuilder strBuilder;
    URLSpan span;
    //PhotoViewAttacher mAttacher;
    ImageView newsImage;
    TextView newsTitle, newsCategory, newsTime, newsPerson, newsParagraph, newsRelated, commentsfb;
    String pnewsTitle, pnewsTime, pnewsCategory, pparagraph, pdetailsUrl, pnewsRelatedTitle, pnewsRealatedUrl, pImgUrl, pnewsAuthor, api_url, pnewsRelated;
    ProgressBar progressBar;
    int id, pageNo, pnewsRelatedId;
    RequestQueue requestQueue;
    String bpnewsTitle, bpnewsTime, bpnewsCategory, bpparagraph, bpdetailsUrl, bpImgUrl, id_url;
    WebView mWebview;
    private SwipeRefreshLayout swipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //initializing
        newsImage = (ImageView) findViewById(R.id.newsImage);
        newsTitle = (TextView) findViewById(R.id.newsTitle);
        newsCategory = (TextView) findViewById(R.id.newsCategory);
        newsTime = (TextView) findViewById(R.id.newsTime);
        newsPerson = (TextView) findViewById(R.id.newsPerson);
        newsParagraph = (TextView) findViewById(R.id.newsParagraph);
        newsRelated = (TextView) findViewById(R.id.related1);
        progressBar = (ProgressBar) findViewById(R.id.loading);
        commentsfb = (TextView) findViewById(R.id.commentsfb);
        //progressBar.setVisibility(View.VISIBLE);


        //getting values
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            bpImgUrl = extras.getString("imgUrl");
            bpdetailsUrl = extras.getString("detailsUrl");
            bpparagraph = extras.getString("paragraph");
            bpnewsTime = extras.getString("newsTime");
            bpnewsTitle = extras.getString("newsTitle");
            id = extras.getInt("id");
            base_url = extras.getString("base_url");


            //constructing the api url
            api_url = base_url + id;


            //downloading data
            //swiping
            swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);
            swipe.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
            swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    refreshContent();
                }
            });


            //swiping
            requestQueue = VolleySingleton.getInstance().getRequestQueue();
            //cached volley
            CacheRequest cacheRequest = new CacheRequest(0, api_url, new Response.Listener<NetworkResponse>() {
                @Override
                public void onResponse(NetworkResponse response) {
                    try {
                        final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));

                        parseResult(jsonString);
                        //Toast.makeText(NewsDetails.this, api_url, Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.GONE);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.GONE);

                    if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof AuthFailureError || error instanceof ServerError
                            || error instanceof NetworkError || error instanceof ParseError) {
                        Toast.makeText(NewsDetails.this, "limited connection ", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(NewsDetails.this, "problem fetching data ", Toast.LENGTH_LONG).show();
                    }

                }
            });

            // Add the request to the RequestQueue.
            requestQueue.add(cacheRequest);
        } else {
            Toast.makeText(NewsDetails.this, "problem", Toast.LENGTH_SHORT).show();
        }
    }


    private void refreshContent() {

        CacheRequest refresh = new CacheRequest(0, api_url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    parseResult(jsonString);

                    progressBar.setVisibility(View.GONE);
                    swipe.setRefreshing(false);
                    Toast.makeText(NewsDetails.this, "content refreshed", Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                swipe.setRefreshing(false);

                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof AuthFailureError || error instanceof ServerError
                        || error instanceof NetworkError || error instanceof ParseError) {
                    Toast.makeText(NewsDetails.this, "No connection ", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(NewsDetails.this, "problem fetching data ", Toast.LENGTH_LONG).show();
                }

            }
        });

        // Add the request to the RequestQueue.
        swipe.setRefreshing(false);
        requestQueue.add(refresh);
    }

    //parsing
    public void parseResult(String jsonString) {
        try {

            if (jsonString != null) {
                JSONObject blogdata = new JSONObject(jsonString);
                pnewsTitle = blogdata.optString("title");
                pnewsCategory = blogdata.optString("pic_caption");
                pnewsTime = blogdata.optString("publish_time");
                pnewsAuthor = blogdata.optString("author");
                pdetailsUrl = blogdata.optString("url");
                pparagraph = blogdata.optString("text");
                pImgUrl = blogdata.optString("pic");
                id_url = blogdata.optString("id_url");
                id = blogdata.optInt("id");
                pageNo = 1;

                //related article
                if (blogdata.has("related")) {
                    JSONObject related = blogdata.optJSONObject("related");

                    pnewsRelatedTitle = related.getString("title");
                    pnewsRealatedUrl = related.optString("url");
                    pnewsRelatedId = related.getInt("id");
                } else {
                    pnewsRelatedTitle = "Related articles";
                    pnewsRealatedUrl = "http://futaa.com/news";
                    pnewsRelatedId = 114700;
                }
            } else {
                pnewsTitle = bpnewsTitle;
                pnewsCategory = bpnewsCategory;
                pnewsTime = bpnewsTime;
                pnewsAuthor = "Futaa staff";
                pdetailsUrl = bpdetailsUrl;
                pparagraph = bpparagraph;
                pImgUrl = bpImgUrl;
                id = 114700;
                pageNo = 1;
                pnewsRelatedTitle = "";
                pnewsRealatedUrl = "http://futaa.com";
                pnewsRelatedId = 114700;

            }
        } catch (JSONException ex) {
            //
        }


        //parse the data
        //allocate to variables.
        //glide
        Glide.with(this).load(pImgUrl)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                }).override(800, 530)
                .centerCrop()
                .crossFade()
                .placeholder(R.drawable.placeholder)
                .into(newsImage);
        //glide


        newsTitle.setText(pnewsTitle);
        newsPerson.setText(pnewsAuthor);
        newsTime.setText(pnewsTime);
        newsCategory.setText(pnewsCategory);
        newsRelated.setText(pnewsRelatedTitle);
        newsRelated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent readOnline = new Intent(NewsDetails.this, ReadOnline.class);
                Bundle b = new Bundle();
                b.putString("detailsUrl", "http://futaa.com"+pnewsRealatedUrl);
                readOnline.putExtras(b);
                startActivity(readOnline);
            }
        });
        //pparagraph

        //setTextViewHTML(newsParagraph, pparagraph);

        newsParagraph.setText(Html.fromHtml(pparagraph));
        newsParagraph.setMovementMethod(LinkMovementMethod.getInstance());
        newsParagraph.setLinksClickable(true);
        newsParagraph.setFocusable(true);

    }


    //menus
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_comments, menu);
        return true;
    }

//end of handling links

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

        }
        if (id == R.id.comments_cloud) {

        }
        return super.onOptionsItemSelected(item);
    }

    //caching class
    private class CacheRequest extends Request<NetworkResponse> {
        private final Response.Listener<NetworkResponse> mListener;
        private final Response.ErrorListener mErrorListener;

        public CacheRequest(int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, errorListener);
            this.mListener = listener;
            this.mErrorListener = errorListener;
        }


        @Override
        protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
            Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
            if (cacheEntry == null) {
                cacheEntry = new Cache.Entry();
            }
            final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
            final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
            long now = System.currentTimeMillis();
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;
            cacheEntry.data = response.data;
            cacheEntry.softTtl = softExpire;
            cacheEntry.ttl = ttl;
            String headerValue;
            headerValue = response.headers.get("Date");
            if (headerValue != null) {
                cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            headerValue = response.headers.get("Last-Modified");
            if (headerValue != null) {
                cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }
            cacheEntry.responseHeaders = response.headers;
            return Response.success(response, cacheEntry);
        }

        @Override
        protected void deliverResponse(NetworkResponse response) {
            mListener.onResponse(response);
        }

        @Override
        protected VolleyError parseNetworkError(VolleyError volleyError) {
            return super.parseNetworkError(volleyError);
        }

        @Override
        public void deliverError(VolleyError error) {
            mErrorListener.onErrorResponse(error);
        }
    }


}
